<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProsesSKHaksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proses_s_k_haks', function (Blueprint $table) {
            $table->id();
            $table->foreignId('pengajuan')->constraint('data_pengajuans');
            $table->string('rekening')->nullable();
            $table->date('penjadwalan')->nullable();
            $table->string('penelitian')->nullable();
            $table->string('bukti_bayar')->nullable();
            $table->string('sk_hak')->nullable();
            $table->string('keterangan')->nullable();
            $table->string('status', 1)->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proses_s_k_haks');
    }
}
