<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNomorBerkasToDataPengajuansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('data_pengajuans', function (Blueprint $table) {
            $table->string('nomor_berkas_peta_bidang')->nullable();
            $table->string('nomor_berkas_sk_hak')->nullable();
            $table->string('nomor_berkas_sertifikat')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('data_pengajuans', function (Blueprint $table) {
            $table->dropColumn('nomor_berkas_peta_bidang');
            $table->dropColumn('nomor_berkas_sk_hak');
            $table->dropColumn('nomor_berkas_sertifikat');
        });
    }
}
