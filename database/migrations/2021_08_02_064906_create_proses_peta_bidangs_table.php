<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProsesPetaBidangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proses_peta_bidangs', function (Blueprint $table) {
            $table->id();
            $table->foreignId('pengajuan')->constraint('data_pengajuans');
            $table->string('rekening')->nullable();
            $table->date('penjadwalan')->nullable();
            $table->string('pengukuran')->nullable();
            $table->string('bukti_bayar')->nullable();
            $table->string('peta_bidang')->nullable();
            $table->string('keterangan')->nullable();
            $table->string('status', 1)->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proses_peta_bidangs');
    }
}
