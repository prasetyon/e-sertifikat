<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MoveNomorBerkas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('data_pengajuans', function (Blueprint $table) {
            $table->dropColumn('nomor_berkas');
        });
        Schema::table('proses_peta_bidangs', function (Blueprint $table) {
            $table->string('nomor_berkas')->nullable();
        });
        Schema::table('proses_s_k_haks', function (Blueprint $table) {
            $table->string('nomor_berkas')->nullable();
        });
        Schema::table('proses_sertifikats', function (Blueprint $table) {
            $table->string('nomor_berkas')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('data_pengajuans', function (Blueprint $table) {
            $table->string('nomor_berkas')->nullable();
        });
        Schema::table('proses_peta_bidangs', function (Blueprint $table) {
            $table->dropColumn('nomor_berkas');
        });
        Schema::table('proses_s_k_haks', function (Blueprint $table) {
            $table->dropColumn('nomor_berkas');
        });
        Schema::table('proses_sertifikats', function (Blueprint $table) {
            $table->dropColumn('nomor_berkas');
        });
    }
}
