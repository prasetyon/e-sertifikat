<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataPengajuansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_pengajuans', function (Blueprint $table) {
            $table->id();
            $table->string('nama');
            $table->double('luas');
            $table->double('nilai');
            $table->string('alamat');
            $table->string('tahun', 4);
            $table->string('kode_barang')->nullable();
            $table->string('kode_registrasi')->nullable();
            $table->string('rencana')->nullable();
            $table->string('hak')->nullable();
            $table->string('nomor_sertifikat')->nullable();
            $table->date('tanggal_sertifikat')->nullable();
            $table->string('pj')->nullable();
            $table->string('keterangan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_pengajuans');
    }
}
