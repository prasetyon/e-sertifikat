@extends('admin')

@section('content')
    <div class="container-fluid">

        <!-- Info boxes -->
        <div class="row">
            <div class="col-12">
                <div class="card card-primary card-outline">
                    <div class="card-body p-0">
                        <table class="table table-striped table-valign-middle">
                            <thead>
                                <tr>
                                    <th style="text-align:center">Pendaftar</th>
                                    <th style="text-align:center">Tanggal Masuk</th>
                                    <th style="text-align:center">Pendaftaran <br/> Peta Bidang</th>
                                    <th style="text-align:center">Pendaftaran <br/> SK HAK</th>
                                    <th style="text-align:center">Pendaftaran <br/> Sertifikat</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td style="text-align:center">
                                        {{-- <img src="dist/img/default-150x150.png" alt="Product 1" class="img-circle img-size-32 mr-2"> --}}
                                        Some Product
                                    </td>
                                    <td style="text-align:center">20 Jun 2021</td>
                                    <td style="text-align:center">
                                        <small class="text-success mr-1">
                                            <i class="fas fa-check"></i>
                                        </small>
                                    </td>
                                    <td style="text-align:center">
                                        <small class="text-danger mr-1">
                                            <i class="fas fa-times"></i>
                                        </small>
                                    </td>
                                    <td style="text-align:center">
                                        <small class="text-danger mr-1">
                                            <i class="fas fa-times"></i>
                                        </small>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="card card-success card-outline">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-6">
                                Pendaftaran Peta Bidang <br/> [selesai pada 25 Juni 2021]
                            </div>
                            <div class="col-6" style="text-align: right">
                                <a href="{{route('form')}}" class="btn btn-outline-success"><i class="fas fa-edit pr-1"></i>Edit Data</a>
                                {{-- <button class="btn btn-outline-success"><i class="fas fa-edit pr-1"></i> Edit Data</button> --}}
                                {{-- <button class="btn btn-outline-primary"><i class="fas fa-upload pr-1"></i> Upload Peta Bidang</button> --}}
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="box box-primary">
                            <div class="box-body">
                                <div class="row">
                                    <p class="col-lg-3 col-sm-6"><b>Nomor Rekening BPN:</b> <br>999 99 2991 922 1<br/><a href="{{asset("upload/buktibayar.pdf")}}" target="_blank">Lihat Bukti Bayar</a><br/></p>
                                    <p class="col-lg-3 col-sm-6"><b>Penjadwalan:</b> <br>22 Juni 2021</p>
                                    <p class="col-lg-3 col-sm-6"><b>Pengukuran:</b> <br>25 m<sup>2</sup> </p>
                                    <p class="col-lg-3 col-sm-6"><b>Peta Bidang:</b> <br><a href="{{asset("upload/buktibayar.pdf")}}" target="_blank">Lihat Peta Bidang</a></p>
                                </div>
                                <p style="white-space:pre-wrap; word-wrap:break-word"><b>Keterangan tambahan</b> <br>Pengukuran dan pemetaan bidang tanah merupakan salah satu rangkaian kegiatan dalam pendaftaran tanah. Kegiatan ini dilakukan dengan cara melakukan pengukuran dan pemetaan pada batas-batas bidang tanah dengan menggunakan metode terestrial, fotogrametris, penginderaan jauh, dan dengan metode-metode lainnya. Namun dengan semakin maju dan berkembangnya ilmu pengetahuan dan teknologi pada saat ini, kegiatan pengukuran dan pemetaan bidang tanah dapat dilakukan dengan menggunakan metode eksterestrial menggunakan receiver GPS yang mempunyai ketelitian tinggi dengan waktu yang relatif singkat.Kegiatan yang dilakukan dalam penenelitian ini adalah pengukuran bidang tanah dengan kriteria kondisi daerah terbuka dan Perumahan menggunakan GNSS metode absolut dan rapid static yang diikatkan pada base station CORS Kota Semarang (CSEM), yang selanjutnya hasil koordinat (X,Y) dan luas dari pengukuran bidang tanah tersebut dibandingkan dengan hasil pengukuran bidang tanah dengan metode terestrial yaitu Total Station. </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card card-warning card-outline">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-6">
                                Pendaftaran SK Hak
                            </div>
                            <div class="col-6" style="text-align: right">
                                <a href="{{route('form')}}" class="btn btn-outline-warning"><i class="fas fa-edit pr-1"></i>Edit Data</a>
                                {{-- <button class="btn btn-outline-warning"><i class="fas fa-edit pr-1"></i> Edit Data</button> --}}
                                <a class="btn btn-outline-primary" data-toggle="modal" data-target="#uploadFile"><i class="fas fa-upload pr-1"></i> Upload SK Hak</a>
                                {{-- <button class="btn btn-outline-primary"><i class="fas fa-upload pr-1"></i> Upload SK Hak</button> --}}
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="box box-primary">
                            <div class="box-body">
                                <p><b>Nomor Rekening BPN:</b> <br>999 99 2991 922 1<br/><a href="{{asset("upload/buktibayar.pdf")}}" target="_blank">Lihat Bukti Bayar</a><br/></p>
                                <p><b>Penjadwalan Penelitian A:</b> <br>30 Juni 2021</p>
                                <p><b>Penelitian A:</b> <br> </p>
                                <p style="white-space:pre-wrap; word-wrap:break-word"><b>Keterangan tambahan</b> <br>Pengukuran dan pemetaan bidang tanah merupakan salah satu rangkaian kegiatan dalam pendaftaran tanah. Kegiatan ini dilakukan dengan cara melakukan pengukuran dan pemetaan pada batas-batas bidang tanah dengan menggunakan metode terestrial, fotogrametris, penginderaan jauh, dan dengan metode-metode lainnya. Namun dengan semakin maju dan berkembangnya ilmu pengetahuan dan teknologi pada saat ini, kegiatan pengukuran dan pemetaan bidang tanah dapat dilakukan dengan menggunakan metode eksterestrial menggunakan receiver GPS yang mempunyai ketelitian tinggi dengan waktu yang relatif singkat.Kegiatan yang dilakukan dalam penenelitian ini adalah pengukuran bidang tanah dengan kriteria kondisi daerah terbuka dan Perumahan menggunakan GNSS metode absolut dan rapid static yang diikatkan pada base station CORS Kota Semarang (CSEM), yang selanjutnya hasil koordinat (X,Y) dan luas dari pengukuran bidang tanah tersebut dibandingkan dengan hasil pengukuran bidang tanah dengan metode terestrial yaitu Total Station. </p>
                                <p><b>SK Hak:</b> <br>Dokumen belum diunggah</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card card-danger card-outline">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-6">
                                Pendaftaran Sertifikat
                            </div>
                            <div class="col-6" style="text-align: right">
                                <a href="{{route('form')}}" class="btn btn-outline-danger"><i class="fas fa-edit pr-1"></i>Rekam Data</a>
                                {{-- <button class="btn btn-outline-danger"><i class="fas fa-edit pr-1"></i> Masukkan Data</button> --}}
                                {{-- <button class="btn btn-outline-primary"><i class="fas fa-upload pr-1"></i> Upload Peta Hak</button> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="uploadFile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form method="post" action="" enctype="multipart/form-data">
                @csrf
                <input name="status" type="hidden" value="proposal" required="required">
                <div class="modal-content">
                    <div class="modal-header">
                        Pilih File
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <input type="file" name="file" required="required">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
