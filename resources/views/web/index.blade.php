@extends('admin')

@include('layout.function')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-2 col-12">
                @livewire('info-bar-component', ['data' => $data])
            </div>

            <div class="col-lg-4 col-12">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                    <h3 class="card-title" style="text-align: center">10 Pengajuan Terakhir</h3>
                                    <div class="text-center" style="width:auto; float:right" >
                                        @livewire('import-pengajuan')
                                    </div>
                            </div>
                            <div class="card-body">
                                <table class="table table-striped table-bordered table-valign-middle">
                                    <thead>
                                        <tr>
                                            <th style="text-align:center">Nomor Berkas</th>
                                            <th style="text-align:center">Nama Aset</th>
                                            <th style="text-align:center">Tanggal Masuk</th>
                                            <th style="text-align:center">Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($data['pengajuan'] as $d)
                                        <tr>
                                            <td style="text-align: left;">{{ $d->kode_barang ?? "-" }}<br/>No. Reg: {{ $d->kode_registrasi ?? "-" }}</td>
                                            <td style="text-align: center">{{ $d->nama }}</td>
                                            <td style="text-align: center;">{{ formatDate($d->created_at) }}</td>
                                            <td style="text-align: center">{{ $d->getStatus() }}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-12">
                <div class="row">
                    <div class="col-lg-7 col-12">
                        <!-- PIE CHART -->
                        <div class="card card-danger">
                            <div class="card-body">
                                <canvas id="jenisChart" style="height: 328px;"></canvas>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>

                    <div class="col-lg-5 col-12">
                        <!-- PIE CHART -->
                        <div class="card card-danger">
                            <div class="card-body">
                                <canvas id="prosesChart" style="height: 328px;"></canvas>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>


                    <div class="col-lg-12 col-12">
                        <!-- PIE CHART -->
                        <div class="card card-danger">
                            <div class="card-body">
                                <canvas id="barChart" style="height: 451px; width:100%"></canvas>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
