@extends('admin', ['title' => 'Pengajuan', 'menu' => 'pengajuan'])

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            @livewire('pengajuan-index-component')
        </div>
    </div>
</div>
@endsection
