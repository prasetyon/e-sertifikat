<div class="card">
    <div class="card-header">
        <button wire:click="closeModal()" class="btn btn-secondary"><i class="fas fa-angle-left pr-1"></i> Back</button>
    </div>
    <form>
        <div class="card-body">
            <div class="box box-primary">
                <div class="box-body">
                    <div class="form-group row">
                        <div class="col-lg-2">
                            <label>Nama Aset</label>
                        </div>
                        <div class="col-lg-10">
                            <input type="text" wire:model="input_nama" id="input_nama" class="form-control @error('input_nama') is-invalid @enderror">
                            @error('input_nama') <div class="invalid-feedback">{{ $message }}</div> @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-2">
                            <label>Luas Aset</label>
                        </div>
                        <div class="col-lg-10">
                            <input type="number" min="0.01" step="0.1" wire:model="input_luas" id="input_luas" class="form-control @error('input_luas') is-invalid @enderror">
                            @error('input_luas') <div class="invalid-feedback">{{ $message }}</div> @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-2">
                            <label>Nilai (Rp)</label>
                        </div>
                        <div class="col-lg-10">
                            <input type="number" min="1" step="1" wire:model="input_nilai" id="input_nilai" class="form-control @error('input_nilai') is-invalid @enderror">
                            @error('input_nilai') <div class="invalid-feedback">{{ $message }}</div> @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-2">
                            <label>Alamat</label>
                        </div>
                        <div class="col-lg-10">
                            <textarea wire:model="input_alamat" class="form-control @error('input_alamat') is-invalid @enderror" rows="2"></textarea>
                            @error('input_alamat') <div class="invalid-feedback">{{ $message }}</div> @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-2">
                            <label>Tahun Perolehan</label>
                        </div>
                        <div class="col-lg-10">
                            <input type="text" wire:model="input_tahun" id="input_tahun" class="form-control @error('input_tahun') is-invalid @enderror">
                            @error('input_tahun') <div class="invalid-feedback">{{ $message }}</div> @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-2">
                            <label>Nomor Kode Barang</label>
                        </div>
                        <div class="col-lg-10">
                            <input type="text" wire:model="input_kode_barang" id="input_kode_barang" class="form-control @error('input_kode_barang') is-invalid @enderror">
                            @error('input_kode_barang') <div class="invalid-feedback">{{ $message }}</div> @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-2">
                            <label>Nomor Register</label>
                        </div>
                        <div class="col-lg-10">
                            <input type="text" wire:model="input_kode_registrasi" id="input_kode_registrasi" class="form-control @error('input_kode_registrasi') is-invalid @enderror">
                            @error('input_kode_registrasi') <div class="invalid-feedback">{{ $message }}</div> @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-2">
                            <label>Tanggal Sertifikat Tanah</label>
                        </div>
                        <div class="col-lg-10">
                            <input type="date" wire:model="input_tanggal_sertifikat" class="form-control @error('input_tanggal_sertifikat') is-invalid @enderror">
                            @error('input_tanggal_sertifikat') <div class="invalid-feedback">{{ $message }}</div> @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-2">
                            <label>Nomor Sertifikat Tanah</label>
                        </div>
                        <div class="col-lg-10">
                            <input type="text" wire:model="input_nomor_sertifikat" id="input_nomor_sertifikat" class="form-control @error('input_nomor_sertifikat') is-invalid @enderror">
                            @error('input_nomor_sertifikat') <div class="invalid-feedback">{{ $message }}</div> @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-2">
                            <label>SKPD Penanggungjawab</label>
                        </div>
                        <div class="col-lg-10">
                            <input type="text" wire:model="input_pj" id="input_pj" class="form-control @error('input_pj') is-invalid @enderror">
                            @error('input_pj') <div class="invalid-feedback">{{ $message }}</div> @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-2">
                            <label>Keterangan Tambahan</label>
                        </div>
                        <div class="col-lg-10">
                            <textarea wire:model="input_keterangan" class="form-control @error('input_keterangan') is-invalid @enderror" rows="10"></textarea>
                            @error('input_keterangan') <div class="invalid-feedback">{{ $message }}</div> @enderror
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer text-right">
            <button type="reset" class="btn btn-danger">Reset</button>
            <button type="button" wire:click.prevent="submitPengajuan()" class="btn btn-success">Save</button>
        </div>
    </form>
</div>
