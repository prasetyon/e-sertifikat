<div class="card">
    <div class="card-header">
        <button wire:click="resetDetailFields()" class="btn btn-secondary"><i class="fas fa-angle-left pr-1"></i> Back</button>
    </div>
    <form>
        <div class="card-body">
            <div class="box box-primary">
                <div class="box-body">
                    <div class="form-group row">
                        <div class="col-lg-2">
                            <label style="width: 100%;">Nomor Berkas</label>
                        </div>
                        <div class="col-lg-10">
                            <input type="text" wire:model="input_nomor_berkas" id="input_nomor_berkas" class="form-control @error('input_nomor_berkas') is-invalid @enderror">
                            @error('input_nomor_berkas') <div class="invalid-feedback">{{ $message }}</div> @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-2">
                            <label style="width: 100%;">Nomor Rekening BPN</label>
                        </div>
                        <div class="col-lg-10">
                            <input type="text" wire:model="input_rekening" id="input_rekening" class="form-control @error('input_rekening') is-invalid @enderror">
                            @error('input_rekening') <div class="invalid-feedback">{{ $message }}</div> @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-2">
                            <label style="width: 100%;">Bukti Bayar</label>
                        </div>
                        <div class="col-lg-10">
                            <input type="file" wire:model="input_bukti_bayar" id="input_bukti_bayar" accept=".pdf" class="form-control @error('input_bukti_bayar') is-invalid @enderror">
                            @error('input_bukti_bayar') <div class="invalid-feedback">{{ $message }}</div> @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-2">
                            <label style="width: 100%;">Tanggal Penjadwalan</label>
                        </div>
                        <div class="col-lg-10">
                            <input type="date" wire:model="input_penjadwalan" class="form-control @error('input_penjadwalan') is-invalid @enderror">
                            @error('input_penjadwalan') <div class="invalid-feedback">{{ $message }}</div> @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-2">
                            <label style="width: 100%;">Pengukuran</label>
                        </div>
                        <div class="col-lg-10">
                            <textarea wire:model="input_pengukuran" class="form-control @error('input_pengukuran') is-invalid @enderror" rows="3"></textarea>
                            @error('input_pengukuran') <div class="invalid-feedback">{{ $message }}</div> @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-2">
                            <label style="width: 100%;">Keterangan Tambahan</label>
                        </div>
                        <div class="col-lg-10">
                            <textarea wire:model="input_keterangan" class="form-control @error('input_keterangan') is-invalid @enderror" rows="5"></textarea>
                            @error('input_keterangan') <div class="invalid-feedback">{{ $message }}</div> @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-2">
                            <label style="width: 100%;">Dokumen Peta Bidang</label>
                        </div>
                        <div class="col-lg-10">
                            <input type="file" wire:model="input_dokumen" id="input_dokumen" accept=".pdf" class="form-control @error('input_dokumen') is-invalid @enderror">
                            @error('input_dokumen') <div class="invalid-feedback">{{ $message }}</div> @enderror
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer text-right">
            <button type="reset" class="btn btn-danger">Reset</button>
            <button type="button" wire:click.prevent="submitPetaBidang()" class="btn btn-success">Save</button>
        </div>
    </form>
</div>
