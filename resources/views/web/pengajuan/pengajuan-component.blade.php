<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            @include('layout.function')
            @if($isOpenPengajuan)
                @include('web.pengajuan.pengajuan')
            @elseif($isOpenSKHak)
                @include('web.pengajuan.skhak')
            @elseif($isOpenSertifikat)
                @include('web.pengajuan.sertifikat')
            @elseif($isOpenPetaBidang)
                @include('web.pengajuan.petabidang')
            @elseif($isOpenDetail)
            @include('web.pengajuan.detail')
            @else
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-3">
                            <a href="{{ route('pengajuan') }}" class="btn btn-dark"><i class="fas fa-refresh pr-1"></i> Reset Search</a>
                            <button wire:click="openPengajuan()" class="btn btn-warning"><i class="fas fa-plus pr-1"></i> Add New</button>
                        </div>
                        <div class="col-6">
                            <input type="text" wire:model="search" placeholder="Search Something..." class="form-control">
                        </div>
                        <div class="col-3">
                            <div style="float: right">
                            @livewire('import-pengajuan')
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover center-header">
                            <thead>
                                <tr>
                                    <th rowspan="2">No</th>
                                    <th rowspan="2">Nama Aset</th>
                                    <th rowspan="2">Luas</th>
                                    <th rowspan="2">Nilai</th>
                                    <th rowspan="2">Tahun<br>Perolehan</th>
                                    <th rowspan="2">Nomor</th>
                                    <th colspan="2">Sertifikat</th>
                                    <th rowspan="2">SKPD<br>Penanggung<br>Jawab</th>
                                    <th rowspan="2">Tanggal Masuk</th>
                                    <th rowspan="2">Proses Peta Bidang</th>
                                    <th rowspan="2">Proses SK HAK Pakai</th>
                                    <th rowspan="2">Proses Sertifikat</th>
                                    <th rowspan="2">Status</th>
                                    <th rowspan="2" width="8%"></th>
                                </tr>
                                <tr>
                                    <th>Tanggal</th>
                                    <th>Nomor</th>
                                </tr>
                            </thead>
                            <tbody class="text-center">
                                @forelse($lists as $list)
                                <tr>
                                    <td>{{ 10*($lists->currentPage()-1)+$loop->iteration}}</td>
                                    <td class="text-left">{{ $list->nama }}<hr style="margin: 5px 1px">{{ $list->alamat }}</td>
                                    <td style="text-align: right;">{{ formatArea($list->luas) }}m<sup>2</sup></td>
                                    <td style="text-align: right;">{{ formatRupiah($list->nilai) }}</td>
                                    <td style="text-align: center;">{{ $list->tahun }}</td>
                                    <td style="text-align: left;">Kode barang :<br>{{ $list->kode_barang }}<hr style="margin: 5px 1px">Register :<br>{{ $list->kode_registrasi }}</td>
                                    <td style="text-align: center;">{{ formatDate($list->tanggal_sertifikat) }}</td>
                                    <td style="text-align: center;">{{ $list->nomor_sertifikat }}</td>
                                    <td style="text-align: left;">{{ $list->pj }}</td>
                                    <td style="text-align: left;">{{ formatDate($list->created_at) }}</td>
                                    <td style="text-align:center">
                                        @if($list->petaBidang && ($list->petaBidang->peta_bidang || $list->petaBidang->rekening))
                                        <small class="text-success mr-1">
                                            <i class="fas fa-check"></i>
                                        </small>
                                        <p style="white-space:pre-wrap; word-wrap:break-word">{{ $list->petaBidang->nomor_berkas }}</p>
                                        @else
                                        <small class="text-warning mr-1">
                                            <i class="fas fa-minus"></i>
                                        </small>
                                        @endif
                                    </td>
                                    <td style="text-align:center">
                                        @if($list->skHak && ($list->skHak->sk_hak || $list->skHak->rekening))
                                        <small class="text-success mr-1">
                                            <i class="fas fa-check"></i>
                                        </small>
                                        <p style="white-space:pre-wrap; word-wrap:break-word">{{ $list->skHak->nomor_berkas }}</p>
                                        @else
                                        <small class="text-warning mr-1">
                                            <i class="fas fa-minus"></i>
                                        </small>
                                        @endif
                                    </td>
                                    <td style="text-align:center">
                                        @if($list->sertifikat && ($list->sertifikat->sertifikat || $list->sertifikat->rekening))
                                        <small class="text-success mr-1">
                                            <i class="fas fa-check"></i>
                                        </small>
                                        <p style="white-space:pre-wrap; word-wrap:break-word">{{ $list->sertifikat->nomor_berkas }}</p>
                                        @else
                                        <small class="text-warning mr-1">
                                            <i class="fas fa-minus"></i>
                                        </small>
                                        @endif
                                    </td>
                                    <td style="text-align:center">
                                        @if($list->status == 2)
                                        <small class="text-success mr-1">
                                            <i class="fas fa-check"></i>
                                        </small>
                                        @elseif($list->status == 1)
                                        <small class="text-warning mr-1">
                                            <i class="fas fa-spinner"></i>
                                        </small>
                                        @else
                                        <small class="text-danger mr-1">
                                            <i class="fas fa-times"></i>
                                        </small>
                                        @endif
                                    </td>
                                    <td style="text-align: center;">
                                        <button wire:click="openDetail({{ $list->id }})" class="btn btn-sm btn-primary" style="width:35px; margin: 2px"><i class="fas fa-eye"></i></button>
                                        <button wire:click="openPengajuan({{ $list->id }})" class="btn btn-sm btn-info" style="width:35px; margin: 2px"><i class="fas fa-edit"></i></button>
                                        @if($list->status !== '2') <button wire:click="approve({{ $list->id }})" class="btn btn-sm btn-success" style="width:35px; margin: 2px" onclick="confirm('Are you sure to approve?') || event.stopImmediatePropagation()"><i class="fas fa-check"></i></button>@endif
                                        <button wire:click="delete({{ $list->id }})" class="btn btn-sm btn-danger" style="width:35px; margin: 2px" onclick="confirm('Are you sure to delete?') || event.stopImmediatePropagation()"><i class="fas fa-trash"></i></button>
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="17">No Data Available</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer">
                    @include('layout.tablecountinfo')
                    <div class="text-xs" style="float: right">
                    @if($lists->hasPages())
                        {{ $lists->links()}}
                    @endif
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
