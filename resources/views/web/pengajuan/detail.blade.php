<div class="container-fluid">
    <!-- Info boxes -->
    <div class="row">
        <div class="col-12" style="margin-bottom: 10px">
            <button wire:click="closeModal()" class="btn btn-secondary"><i class="fas fa-angle-left pr-1"></i> Back</button>
        </div>

        <div class="col-12">
            <div class="card">
                <div class="card-body p-0">
                    <table class="table table-striped table-valign-middle text-center">
                        <thead class='bg-olive'>
                            <tr>
                                <th>Nama Aset</th>
                                <th>Alamat</th>
                                <th>Luas</th>
                                <th>Nilai</th>
                                <th>Tahun<br>Perolehan</th>
                                <th>Kode Barang</th>
                                <th>Kode Register</th>
                                <th>Nomor Berkas</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr style="background: white">
                                <td>{{ $selectedData->nama }}</td>
                                <td>{{ $selectedData->alamat }}</td>
                                <td>{{ formatArea($selectedData->luas) }}m<sup>2</sup></td>
                                <td>{{ formatRupiah($selectedData->nilai) }}</td>
                                <td>{{ $selectedData->tahun }}</td>
                                <td>{{ $selectedData->kode_barang }}</td>
                                <td>{{ $selectedData->kode_registrasi }}</td>
                                <td>{{ $selectedData->nomor_berkas }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-12">
            <div class="card">
                <div class="card-body p-0">
                    <table class="table table-striped table-valign-middle text-center">
                        <thead class='bg-olive'>
                            <tr>
                                <th>Tanggal Masuk</th>
                                <th>Keterangan</th>
                                <th>Pendaftaran <br/> Peta Bidang</th>
                                <th>Pendaftaran <br/> SK HAK Pakai</th>
                                <th>Pendaftaran <br/> Sertifikat</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr style="background: white">
                                <td>{{ formatDate($selectedData->created_at) }}</td>
                                <td>{{ $selectedData->keterangan }}</td>
                                <td>
                                    @if($selectedData->petaBidang && ($selectedData->petaBidang->peta_bidang || $selectedData->petaBidang->rekening))
                                    <small class="text-success mr-1">
                                        <i class="fas fa-check"></i>
                                    </small>
                                    @else
                                    <small class="text-warning mr-1">
                                        <i class="fas fa-minus"></i>
                                    </small>
                                    @endif
                                </td>
                                <td>
                                    @if($selectedData->skHak && ($selectedData->skHak->sk_hak || $selectedData->skHak->rekening))
                                    <small class="text-success mr-1">
                                        <i class="fas fa-check"></i>
                                    </small>
                                    @else
                                    <small class="text-warning mr-1">
                                        <i class="fas fa-minus"></i>
                                    </small>
                                    @endif
                                </td>
                                <td>
                                    @if($selectedData->sertifikat && ($selectedData->sertifikat->sertifikat || $selectedData->sertifikat->rekening))
                                    <small class="text-success mr-1">
                                        <i class="fas fa-check"></i>
                                    </small>
                                    @else
                                    <small class="text-warning mr-1">
                                        <i class="fas fa-minus"></i>
                                    </small>
                                    @endif
                                </td>
                                <td>
                                    @if($selectedData->status == 2)
                                    <small class="text-success mr-1">
                                        <i class="fas fa-check"></i>
                                    </small>
                                    @elseif($selectedData->status == 1)
                                    <small class="text-warning mr-1">
                                        <i class="fas fa-spinner"></i>
                                    </small>
                                    @else
                                    <small class="text-danger mr-1">
                                        <i class="fas fa-times"></i>
                                    </small>
                                    @endif
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-6">
                            Pendaftaran Peta Bidang @if($selectedData->petaBidang)<br/> [terakhir diubah pada {{ $selectedData->petaBidang->updated_at }}]@endif
                        </div>
                        <div class="col-6" style="text-align: right">
                            @if($selectedData->petaBidang)
                                <button wire:click="openPetaBidang({{ $selectedData->petaBidang }})" class="btn btn-success"><i class="fas fa-edit pr-1"></i>Edit Data</button>
                                <button wire:click="deleteProses({{ $selectedData->id }}, 'petabidang')" class="btn btn-danger" onclick="confirm('Are you sure to delete?') || event.stopImmediatePropagation()"><i class="fas fa-edit pr-1"></i>Hapus Data</button>
                            @else
                                <button wire:click="openPetaBidang()" class="btn btn-success"><i class="fas fa-edit pr-1"></i> Rekam Data</button>
                            @endif
                        </div>
                    </div>
                </div>
                @if($selectedData->petaBidang)
                <div class="card-body">
                    <div class="box box-primary">
                        <div class="box-body">
                            <div class="row">
                                <p class="col-lg-3 col-sm-6">
                                    <b>Nomor Berkas:</b> <br>
                                    {{ $selectedData->petaBidang->nomor_berkas }}
                                </p>
                                <p class="col-lg-3 col-sm-6">
                                    <b>Nomor Rekening BPN:</b> <br>
                                    {{$selectedData->petaBidang->rekening}}<br/>
                                    @if($selectedData->petaBidang->bukti_bayar)
                                    <a href="{{$selectedData->petaBidang->bukti_bayar}}" target="_blank">Lihat Bukti Bayar</a><br/>
                                    @else
                                    Dokumen belum diupload
                                    @endif
                                </p>
                                <p class="col-lg-3 col-sm-6">
                                    <b>Penjadwalan:</b> <br>
                                    {{formatDate($selectedData->petaBidang->penjadwalan)}}
                                </p>
                                <p class="col-lg-3 col-sm-6">
                                    <b>Pengukuran:</b> <br>
                                    {{$selectedData->petaBidang->pengukuran}}
                                </p>
                            </div>
                            <div class="row">
                                <p class="col-lg-3 col-sm-6">
                                    <b>Peta Bidang:</b> <br>
                                    @if($selectedData->petaBidang->peta_bidang)
                                    <a href="{{$selectedData->petaBidang->peta_bidang}}" target="_blank">Lihat Peta Bidang</a>
                                    @else
                                    Dokumen belum diupload
                                    @endif
                                </p>
                                <p class="col-lg-3 col-sm-6">
                                    <b style="white-space:pre-wrap; word-wrap:break-word">Keterangan Tambahan</b> <br>{{$selectedData->petaBidang->keterangan}}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            </div>

            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-6">
                            Pendaftaran SK Hak Pakai @if($selectedData->skHak) <br/> [terakhir diubah pada {{ $selectedData->skHak->updated_at }}] @endif
                        </div>
                        <div class="col-6" style="text-align: right">
                            @if($selectedData->skHak)
                                <button wire:click="openSKHak({{ $selectedData->skHak }})" class="btn btn-success"><i class="fas fa-edit pr-1"></i>Edit Data</button>
                                <button wire:click="deleteProses({{ $selectedData->id }}, 'skhak')" class="btn btn-danger" onclick="confirm('Are you sure to delete?') || event.stopImmediatePropagation()"><i class="fas fa-edit pr-1"></i>Hapus Data</button>
                            @else
                                <button wire:click="openSKHak()" class="btn btn-success"><i class="fas fa-edit pr-1"></i> Rekam Data</button>
                            @endif
                        </div>
                    </div>
                </div>
                @if($selectedData->skHak)
                <div class="card-body">
                    <div class="box box-primary">
                        <div class="box-body">
                            <div class="row">
                                <p class="col-lg-3 col-sm-6">
                                    <b>Nomor Berkas:</b> <br>
                                    {{ $selectedData->skHak->nomor_berkas }}
                                </p>
                                <p class="col-lg-3 col-sm-6">
                                    <b>Nomor Rekening BPN:</b> <br>
                                    {{$selectedData->skHak->rekening}}<br/>
                                    @if($selectedData->skHak->bukti_bayar)
                                    <a href="{{$selectedData->skHak->bukti_bayar}}" target="_blank">Lihat Bukti Bayar</a><br/>
                                    @else
                                    Dokumen belum diupload
                                    @endif
                                </p>
                                <p class="col-lg-3 col-sm-6">
                                    <b>Penjadwalan:</b> <br>
                                    {{formatDate($selectedData->skHak->penjadwalan)}}
                                </p>
                                <p class="col-lg-3 col-sm-6">
                                    <b>Penelitian:</b> <br>
                                    {{$selectedData->skHak->penelitian}}
                                </p>
                            </div>
                            <div class="row">
                                <p class="col-lg-3 col-sm-6">
                                    <b>SK Hak Pakai:</b> <br>
                                    @if($selectedData->skHak->sk_hak)
                                    <a href="{{$selectedData->skHak->sk_hak}}" target="_blank">Lihat SK Hak Pakai</a>
                                    @else
                                    Dokumen belum diupload
                                    @endif
                                </p>
                                <p class="col-lg-3 col-sm-6">
                                    <b style="white-space:pre-wrap; word-wrap:break-word">Keterangan Tambahan</b> <br>{{$selectedData->skHak->keterangan}}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            </div>

            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-6">
                            Pendaftaran Sertifikat @if($selectedData->sertifikat) <br/> [terakhir diubah pada {{ $selectedData->sertifikat->updated_at }}] @endif
                        </div>
                        <div class="col-6" style="text-align: right">
                            @if($selectedData->sertifikat)
                                <button wire:click="openSertifikat({{ $selectedData->sertifikat }})" class="btn btn-success"><i class="fas fa-edit pr-1"></i>Edit Data</button>
                                <button wire:click="deleteProses({{ $selectedData->id }}, 'sertifikat')" class="btn btn-danger" onclick="confirm('Are you sure to delete?') || event.stopImmediatePropagation()"><i class="fas fa-edit pr-1"></i>Hapus Data</button>
                            @else
                                <button wire:click="openSertifikat()" class="btn btn-success"><i class="fas fa-edit pr-1"></i> Rekam Data</button>
                            @endif
                        </div>
                    </div>
                </div>
                @if($selectedData->sertifikat)
                <div class="card-body">
                    <div class="box box-primary">
                        <div class="box-body">
                            <div class="row">
                                <p class="col-lg-3 col-sm-6">
                                    <b>Nomor Berkas:</b> <br>
                                    {{ $selectedData->sertifikat->nomor_berkas }}
                                </p>
                                <p class="col-lg-3 col-sm-6">
                                    <b>Nomor Rekening BPN:</b> <br>
                                    {{$selectedData->sertifikat->rekening}}<br/>
                                    @if($selectedData->sertifikat->bukti_bayar)
                                    <a href="{{$selectedData->sertifikat->bukti_bayar}}" target="_blank">Lihat Bukti Bayar</a><br/>
                                    @else
                                    Dokumen belum diupload
                                    @endif
                                </p>
                                <p class="col-lg-3 col-sm-6">
                                    <b>Sertifikat:</b> <br>
                                    @if($selectedData->sertifikat->sertifikat)
                                    <a href="{{$selectedData->sertifikat->sertifikat}}" target="_blank">Lihat Sertifikat</a>
                                    @else
                                    Dokumen belum diupload
                                    @endif
                                </p>
                                <p class="col-lg-3 col-sm-6">
                                    <b style="white-space:pre-wrap; word-wrap:break-word">Keterangan Tambahan</b> <br>{{$selectedData->sertifikat->keterangan}}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="uploadFile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form method="post" action="" enctype="multipart/form-data">
            @csrf
            <input name="status" type="hidden" value="proposal" required="required">
            <div class="modal-content">
                <div class="modal-header">
                    Pilih File
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <input type="file" name="file" required="required">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>
