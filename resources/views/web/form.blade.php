@extends('admin')

@section('content')
    <div class="container-fluid">

        <!-- Info boxes -->
        <div class="row">
            <div class="col-12">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <div class="row">
                            Masukkan Data Pendaftaran Peta Bidang
                        </div>
                    </div>
                    <div class="card-body">
                        <form class="col-lg-12" role="form" action="" method="post" enctype="multipart/form-data">
                            <div class="box box-primary">
                                <div class="box-body">
                                    <div class="form-group row">
                                        <div class="col-lg-2">
                                            <label style="width: 100%;">Nomor Rekening BPN</label>
                                        </div>
                                        <div class="col-lg-10">
                                            <input type="text" id="editprodi" class="form-control" name="nama" required="required">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-2">
                                            <label style="width: 100%;">Bukti Bayar</label>
                                        </div>
                                        <div class="col-lg-10">
                                            <input type="file" accept="application/pdf" id="file" name="file" placeholder="" style="width:100%" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-2">
                                            <label style="width: 100%;">Pendaftar</label>
                                        </div>
                                        <div class="col-lg-10">
                                            <select class="form-control select2" name="jam" required>
                                                <option value="0"> --  Pilih Pendaftar --</option>
                                                    <option value="">H. Toni Raharja</option>
                                                    <option value="">Budi Sulaiman</option>
                                                    <option value="">Anggun C. Sasmi</option>
                                                    <option value="">Lionel Messi</option>
                                                    <option value="">Selena Gomez</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-2">
                                            <label style="width: 100%;">Tanggal Penjadwalan</label>
                                        </div>
                                        <div class="col-lg-10">
                                            <input type="date" name="tanggal" id="tanggal" class="form-control" data-inputmask-alias="date"
                                                data-inputmask-inputformat="dd/mm/yyyy" data-mask>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-2">
                                            <label style="width: 100%;">Pengukuran</label>
                                        </div>
                                        <div class="col-lg-10">
                                            <input type="text" id="editprodi" class="form-control" name="nama" required="required">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-lg-2">
                                            <label style="width: 100%;">Keterangan Tambahan</label>
                                        </div>
                                        <div class="col-lg-10">
                                            <textarea type="text" class="form-control" id="judul" name="judul" placeholder="" rows="5" style="width:100%" required>{{old('judul')}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- <button type="submit" class="btn btn-success" >Simpan</button> --}}
                            <a href="{{route('document')}}" class="btn btn-success" style="float: right;">Simpan</a>
                            <a href="{{route('document')}}" class="btn btn-warning" style="float: right; margin-right: 5px">Kembali</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
