<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{url('/')}}" class="brand-link">
        <img src="{{asset('logojombang.png')}}" alt="BPKAD" class="brand-image elevation-3"
             style="opacity: .8">
        <span class="brand-text font-weight-light">BPKAD Jombang</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <!-- <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="info">
            <a href="#" class="d-block">{{Session::get('name')}}</a>
            </div>
        </div> -->

        <!-- Sidebar Menu-->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="{{route('dashboard')}}" class="nav-link @if($menu=='dashboard') active @endif">
                        <i class="nav-icon fa fa-home"></i>
                        <p>
                            Dashboard
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('pengajuan')}}" class="nav-link @if($menu=='pengajuan') active @endif">
                        <i class="nav-icon fa fa-file"></i>
                        <p>
                            Pengajuan
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
      <!-- /.sidebar-menu -->
    </div>
  <!-- /.sidebar -->
</aside>
