<footer class="main-footer">
    <strong><a href="https://www.linkedin.com/company/ezlifedeveloper" target="_blank">EZ-Life Developer</a> &copy; 2021 @if(date('Y')>2021) - {{date('Y')}} @endif</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
        <b>Version</b> 1.0.0
    </div>
</footer>
