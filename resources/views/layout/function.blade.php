@php
    function formatRupiah($number)
    {
        return 'Rp'.number_format($number, 0, ",", ".");
    }

    function formatArea($number)
    {
        return number_format($number, 0, ",", ".");
    }

    function formatDate($date)
    {
        if (!$date) return null;
        $date = date_create($date);
        return date_format($date, "d M Y");
    }
@endphp
