@if(session('alert-success'))
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <br>
                <div class='alert alert-success alert-dismissible' id="alert">
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                    <h6><i class='icon fa fa-check'></i> {{session('alert-success')}}</h6>
                </div>
            </div>
        </div>
    </div>
@elseif(session('alert'))
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <br>
                <div class='alert alert-danger alert-dismissible' id="alert">
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                    <h6>{{session('alert')}}</h6>
                </div>
            </div>
        </div>
    </div>
@endif
