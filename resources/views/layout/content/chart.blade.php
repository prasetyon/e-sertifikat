<!-- page script -->
<script>
    var prosesData = {
        labels: [
            'Belum Diproses',
            'Dalam Proses',
            'Selesai',
        ],
        datasets: [{
            data: @php echo json_encode($data['rekap']); @endphp,
            backgroundColor : ['#f56954', '#3c8dbc', '#00a65a'],
        }]
    };

    var prosesChartCanvas = $('#prosesChart').get(0).getContext('2d')
    var prosesOptions     = {
        maintainAspectRatio : false,
        responsive : true,
    }

    var prosesChart = new Chart(prosesChartCanvas, {
        type: 'pie',
        data: prosesData,
        options: prosesOptions
    })
</script>

<script>
    var jenisData = {
        labels: @php echo json_encode($data['jenislabel']); @endphp,
        datasets: [{
            data: @php echo json_encode($data['jenisdata']); @endphp,
            backgroundColor : @php echo json_encode($data['jeniscolor']); @endphp,
        }]
    };

    var jenisChartCanvas = $('#jenisChart').get(0).getContext('2d')
    var jenisOptions     = {
        maintainAspectRatio : false,
        responsive : true,
    }

    var jenisChart = new Chart(jenisChartCanvas, {
        type: 'pie',
        data: jenisData,
        options: jenisOptions
    })
</script>

<script>
    const data = {
        labels: @php echo json_encode($data['barlabel']); @endphp,
        datasets: [{
            label: 'Komposisi SKPD > 10',
            data: @php echo json_encode($data['bardata']); @endphp,
            backgroundColor: @php echo json_encode($data['barcolor']); @endphp,
        }]
    };

    var barChartCanvas = $('#barChart').get(0).getContext('2d')
    var barChartData = jQuery.extend(true, {}, data)
    barChartData.datasets[0] = data.datasets[0]

    var barChartOptions = {
        responsive              : true,
        maintainAspectRatio     : false,
        datasetFill             : true,
        scales: {
            y: {
                beginAtZero: true
            },
        }
    }

    var barChart = new Chart(barChartCanvas, {
      type: 'bar',
      data: barChartData,
      options: barChartOptions
    })
</script>
