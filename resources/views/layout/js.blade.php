<!-- jQuery -->
<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>

<!-- Bootstrap 4 -->
<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

<!-- DataTables -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>

<!-- Select2 -->
<script src="{{ asset('plugins/select2/js/select2.full.min.js') }}"></script>

<!-- SweetAlert2 -->
<script src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>

<!-- InputMask -->
<script src="{{ asset('plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('plugins/inputmask/min/jquery.inputmask.bundle.min.js') }}"></script>

<!-- date-range-picker -->
<script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>

<!-- Tempusdominus Bootstrap 4 -->
<script src="{{ asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>

<!-- ChartJS -->
<script src="{{ asset('plugins/chart.js/Chart.min.js') }}"></script>

<!-- AdminLTE App -->
<script src="{{ asset('dist/js/adminlte.js') }}"></script>

<script>
    $(function () {
        //Initialize Select2 Elements
        $('.select2').select2()
    });
    $(function () {
        $("#datatable-full").DataTable({
        "responsive": true,
        "autoWidth": false,
        "sorting": false
        });
    });
    $(function () {
        $("#datatable-full2").DataTable({
        "responsive": true,
        "autoWidth": false,
        "sorting":false
        });
    });
    $(function () {
        $("#datatable-standard").DataTable({
        "responsive": true,
        "autoWidth": false,
        "sorting": false,
        "paging": false,
        });
    });
    $(function () {
        $("#datatable-standard-scrollx").DataTable({
        "responsive": false,
        "autoWidth": false,
        "sorting": false,
        "paging": false,
        "scrollX": true,
        });
    });
    $(function () {
        $("#datatable-standard-report").DataTable({
        "responsive": false,
        "autoWidth": false,
        "sorting": false,
        "paging": false,
        "scrollX": true,
        });
    });
    $(function () {
        $("#datatable-disabled").DataTable({
            "responsive": true,
            "autoWidth": false,
            "sorting": true,
            "paging": false,
            "aoColumnDefs": [
                { "bSortable": false, "aTargets": [ "_all" ] }
            ],
            "bFilter": false
        });
    });

  //Date range picker
  $('#filtermonth').daterangepicker()

  //Datemask dd/mm/yyyy
  $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })

  function openWewenangModal(idma, access) {
    document.getElementById("idma").value = idma;

    var my_value = access ?? "";
    $("#addwewenang").modal()
  };

  function showSuccessAlert(msg) {
    const Toast = Swal.mixin({
      showConfirmButton: false,
      timer: 3000
    });

    Toast.fire({
        icon: 'success',
        title: msg
      })
    };

  function openOtherModal(modal1, modal2) {
    $(modal2).modal("show");
    $(modal1).modal("hide");
  }
</script>

<script>
    $("#alert").fadeTo(2000, 500).slideUp(500, function(){
        $("#alert").slideUp(500);
    });
</script>
