<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('favicon.ico')}}" />

    <title>{{($title??"").' - '.env("APP_NAME")}}</title>

    @include('layout.css')
    @livewireStyles
</head>

<body class="hold-transition sidebar-mini text-sm sidebar-collapse">
    <div class="wrapper">
        @include('layout.header')
        @include('layout.sidebar')
        <div class="content-wrapper">
            @include('layout.content.header')
            <div class="content">
                @include('layout.content.alert')
                @yield('content')
            </div>
        </div>

        @include('layout.footer')
    </div>

    @livewireScripts
    @include('layout.js')
    @if ($menu == 'dashboard') @include('layout.content.chart') @endif
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10">
    </script>
    <x-livewire-alert::scripts />
</body>
</html>
