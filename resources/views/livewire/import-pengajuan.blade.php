<div>
    <button type="button" class="btn btn-success" wire:click='openModalImport'>
        Import Excel
    </button>

    <button type="button" class="btn btn-primary" wire:loading.class="disabled" wire:click.prevent='exportPengajuan'>
        <div wire:loading wire:target="exportPengajuan">
            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
            Exporting Data
        </div>
        <div wire:loading.class="d-none" wire:target="exportPengajuan">Export Excel</div>
    </button>

    @if($showModal)
    <div class="modal d-block" wire:ignore.self id="importExcel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<form wire:ignore='importPengajuan'>
                @csrf
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Import Excel</h5>
					</div>
					<div class="modal-body">
						<label>Format File Import</label>
						<div class="form-group">
                            <a href="{{ asset('IMPORT_TEMPLATE.xlsx') }}" target="_blank">Download Format File</a>
                        </div>

						<label>Pilih file excel</label>
						<div class="form-group">
							<input type="file" wire:model="file" required="required">
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" wire:click="closeModalImport" >Close</button>
						<button type="button" wire:loading.class="disabled" wire:click.prevent="importPengajuan" class="btn btn-primary">
                            <div wire:loading wire:target="importPengajuan">
                                <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                Importing Data
                            </div>
                            <div wire:loading.class="d-none" wire:target="importPengajuan">Import</div>
                        </button>
					</div>
				</div>
			</form>
		</div>
    </div>
    @endif
</div>
