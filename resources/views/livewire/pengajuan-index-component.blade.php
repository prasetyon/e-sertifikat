<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            @if($isOpenPengajuan)
                @include('web.pengajuan.pengajuan')
            @else
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-3">
                            <button wire:click="openPengajuan()" class="btn btn-dark"><i class="fas fa-plus pr-1"></i> Add New</button>
                        </div>
                        <div class="col-6">
                            <input type="text" wire:model="search" placeholder="Search Something..." class="form-control">
                        </div>
                        <div class="col-3">
                            <div style="float: right">
                            @livewire('import-pengajuan')
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover center-header">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Kode Barang</th>
                                    <th>Jumlah Berkas</th>
                                    <th width="8%"></th>
                                </tr>
                            </thead>
                            <tbody class="text-center">
                                @forelse($lists as $list)
                                <tr>
                                    <td>{{ 10*($lists->currentPage()-1)+$loop->iteration}}</td>
                                    <td>{{ $list->kode_barang }}</td>
                                    <td>{{ $list->num }}</td>
                                    <td>
                                        <a href="{{ route('detail', ['kode', $list->kode_barang]) }}" class="btn btn-sm btn-primary" style="width:35px; margin: 2px"><i class="fas fa-eye"></i></a>
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="17">No Data Available</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer">
                    @include('layout.tablecountinfo')
                    <div class="text-xs" style="float: right">
                    @if($lists->hasPages())
                        {{ $lists->links()}}
                    @endif
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
