<div wire:poll.750ms>
    <div class="info-box mb-3">
        <span class="info-box-icon bg-primary elevation-1"><i class="fas fa-file"></i></span>

        <div class="info-box-content">
            <span class="info-box-text">Total Asset</span>
            <span class="info-box-number">{{ $total }}</span>
            <span class="info-box-number"><a style="color: blue" wire:click='openTotal'>[Detail]</a></span>
        </div>
    </div>

    @if($showTotal)
    <div class="row">
        <div class="col-1">
            <div class="info-box bg-success">
            </div>
        </div>
        <div class="col-11">
            <div class="info-box pl-3 mb-3">
                <span class="info-box-icon bg-success elevation-1"><i class="fas fa-check"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Sudah Sertifikat</span>
                    <span class="info-box-number"><a href="{{ route('detail', ['status', '2']) }}">{{ $sudahSertifikat }}</a></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-1">
            <div class="info-box bg-warning">
            </div>
        </div>
        <div class="col-11">
            <div class="info-box pl-3 mb-3">
                <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-times"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Belum Sertifikat</span>
                    <span class="info-box-number">{{ $belumSertifikat }}</span>
                    <span class="info-box-number"><a style="color: blue" wire:click='openSertif'>[Detail]</a></span>
                </div>
            </div>
        </div>
    </div>
    @endif
    @if($showSertif)
    <div class="row">
        <div class="col-1"></div>
        <div class="col-1">
            <div class="info-box bg-danger">
            </div>
        </div>
        <div class="col-10">
            <div class="info-box mb-3">
                <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-minus"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Belum Diproses</span>
                    <span class="info-box-number"><a href="{{ route('detail', ['status', '0']) }}">{{ $belumProses }}</a></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-1"></div>
        <div class="col-1">
            <div class="info-box bg-info">
            </div>
        </div>
        <div class="col-10">
            <div class="info-box mb-3">
                <span class="info-box-icon bg-info elevation-1"><i class="fas fa-spinner"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Sedang Diproses</span>
                    <span class="info-box-number"><a href="{{ route('detail', ['status', '1']) }}">{{ $sedangProses }}</a></span>
                    <span class="info-box-number"><a style="color: blue" wire:click='openProses'>[Detail]</a></span>
                </div>
            </div>
        </div>
    </div>
    @endif
    @if($showProses)
    <div class="row">
        <div class="col-2"></div>
        <div class="col-1">
            <div class="info-box bg-secondary">
            </div>
        </div>
        <div class="col-9">
            <div class="info-box mb-3">
                <span class="info-box-icon bg-secondary elevation-1"><i class="fas fa-map"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Peta Bidang</span>
                    <span class="info-box-number">{{ $petaBidang }}</span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-2"></div>
        <div class="col-1">
            <div class="info-box bg-black">
            </div>
        </div>
        <div class="col-9">
            <div class="info-box mb-3">
                <span class="info-box-icon bg-black elevation-1"><i class="fas fa-pencil-square"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">SK HAK Pakai</span>
                    <span class="info-box-number">{{ $skHak }}</span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-2"></div>
        <div class="col-1">
            <div class="info-box bg-olive">
            </div>
        </div>
        <div class="col-9">
            <div class="info-box mb-3">
                <span class="info-box-icon bg-olive elevation-1"><i class="fas fa-file-pdf"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Sertifikat</span>
                    <span class="info-box-number">{{ $sertifikat }}</span>
                </div>
            </div>
        </div>
    </div>
    @endif
</div>
