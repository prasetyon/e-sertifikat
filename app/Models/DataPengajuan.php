<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataPengajuan extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function petaBidang()
    {
        return $this->hasOne(ProsesPetaBidang::class, 'pengajuan');
    }

    public function sertifikat()
    {
        return $this->hasOne(ProsesSertifikat::class, 'pengajuan');
    }

    public function skHak()
    {
        return $this->hasOne(ProsesSKHak::class, 'pengajuan');
    }

    public function file()
    {
        return $this->hasMany(DokumenPengajuan::class, 'pengajuan');
    }

    public function getStatus()
    {
        if (intval($this->status) == 0) return 'Belum Diproses';
        else if (intval($this->status) == 1) return 'Sedang Diproses';
        else if (intval($this->status) == 2) return 'Selesai Diproses';
    }
}
