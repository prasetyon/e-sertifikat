<?php

namespace App\Imports;

use App\Models\DataPengajuan;
use App\Models\ProsesPetaBidang;
use App\Models\ProsesSertifikat;
use App\Models\ProsesSKHak;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class DataPengajuanImport implements ToModel, WithStartRow
{
    public function model(array $row)
    {
        if ($row[9] && $row[10]) $status = 2;
        else if ($row[13] || $row[17] || $row[21]) $status = 1;
        else $status = 0;

        // if ($row[6] && $row[7]) {
        //     $data = DataPengajuan::updateOrCreate(
        //         [
        //             'kode_barang' => $row[6] ?? '-',
        //             'kode_registrasi' => $row[7] ?? '-',
        //         ],
        //         [
        //             'nama' => $row[1],
        //             'luas' => $row[2],
        //             'nilai' => $row[3] ?? 0,
        //             'alamat' => $row[4] ?? "",
        //             'tahun' => $row[5] ?? "",
        //             'nomor_sertifikat' => $row[9],
        //             'tanggal_sertifikat' => $row[8],
        //             'pj' => $row[11],
        //             'status' => $status,
        //             'keterangan' => $row[10],
        //             'nomor_berkas_peta_bidang' => $row[12],
        //             'nomor_berkas_sk_hak' => $row[17],
        //             'nomor_berkas_sertifikat' => $row[22],
        //         ]
        //     );
        // } else {
        $data = DataPengajuan::create(
            [
                'kode_barang' => $row[6] ?? '-',
                'kode_registrasi' => $row[7] ?? '-',
                'nama' => $row[1],
                'luas' => $row[2],
                'nilai' => $row[3] ?? 0,
                'alamat' => $row[4] ?? "",
                'tahun' => $row[5] ?? "",
                'nomor_sertifikat' => $row[9],
                'tanggal_sertifikat' => $row[8],
                'pj' => $row[11],
                'status' => $status,
                'keterangan' => $row[10],
                'nomor_berkas_peta_bidang' => $row[12],
                'nomor_berkas_sk_hak' => $row[17],
                'nomor_berkas_sertifikat' => $row[22],
            ]
        );
        // }

        $id = $data->id;
        $petabidang = ProsesPetaBidang::updateOrCreate(
            [
                'pengajuan' => $id
            ],
            [
                'nomor_berkas' => $row['12'],
                'rekening' => $row['13'],
                'penjadwalan' => $row['14'],
                'pengukuran' => $row['15'],
                'keterangan' => $row['16'],
            ]
        );
        $skhak = ProsesSKHak::updateOrCreate(
            [
                'pengajuan' => $id
            ],
            [
                'nomor_berkas' => $row['17'],
                'rekening' => $row['18'],
                'penjadwalan' => $row['19'],
                'penelitian' => $row['20'],
                'keterangan' => $row['21'],
            ]
        );
        $sertifikasi = ProsesSertifikat::updateOrCreate(
            [
                'pengajuan' => $id
            ],
            [
                'nomor_berkas' => $row['22'],
                'rekening' => $row['23'],
                'keterangan' => $row['24'],
            ]
        );

        return $data;
    }

    public function startRow(): int
    {
        return 4;
    }
}
