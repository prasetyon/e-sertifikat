<?php

namespace App\Exports;

use App\Helper\Helper;
use App\Models\DataPengajuan;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithMapping;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Cell\DefaultValueBinder;

class DataPengajuanExport extends DefaultValueBinder implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents, WithMapping
{
    use Exportable;

    public function collection()
    {
        return DataPengajuan::orderBy('created_at', 'desc')->get();
    }

    public function bindValue(Cell $cell, $value)
    {
        if (is_numeric($value)) {
            $cell->setValueExplicit($value, DataType::TYPE_STRING);

            return true;
        }

        // else return default behavior
        return parent::bindValue($cell, $value);
    }

    public function map($data): array
    {
        return [
            $data->nama,
            $data->luas,
            $data->nilai,
            $data->alamat,
            $data->tahun,
            $data->kode_barang,
            $data->kode_registrasi,
            $data->nomor_berkas,
            $data->tanggal_sertifikat,
            $data->nomor_sertifikat,
            $data->keterangan,
            $data->pj,
            $data->getStatus(),
            Helper::formatDate($data->created_at),
            $data->petaBidang->rekening ?? "",
            $data->petaBidang->penjadwalan ?? "",
            $data->petaBidang->pengukuran ?? "",
            $data->petaBidang->keterangan ?? "",
            $data->skHak->rekening ?? "",
            $data->skHak->penjadwalan ?? "",
            $data->skHak->penelitian ?? "",
            $data->skHak->keterangan ?? "",
            $data->sertifikat->rekening ?? "",
            $data->sertifikat->keterangan ?? "",
        ];
    }

    public function headings(): array
    {
        return [
            'NAMA ASET',
            'LUAS',
            'NILAI',
            'ALAMAT',
            'TAHUN PEROLEHAN',
            'NOMOR KODE BARANG',
            'NOMOR REGISTER',
            'NOMOR BERKAS',
            'TANGGAL SERTIFIKAT',
            'NOMOR SERTIFIKAT',
            'KETERANGAN',
            'SKPD PENANGGUNGJAWAB',
            'STATUS',
            'TANGGAL_MASUK',
            'REK BPN PROSES PETA BIDANG',
            'PENJADWALAN PROSES PETA BIDANG',
            'PENGUKURAN PROSES PETA BIDANG',
            'KETERANGAN PROSES PETA BIDANG',
            'REK BPN PROSES SK HAK PAKAI',
            'PENJADWALAN PROSES SK HAK PAKAI',
            'PENELITIAN PROSES SK HAK PAKAI',
            'KETERANGAN PROSES SK HAK PAKAI',
            'REK BPN PROSES SERTIFIKAT',
            'KETERANGAN PROSES SERTIFIKAT',
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function ($event) {
                $cellRange = 'A1:N1';
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
            },
        ];
    }
}
