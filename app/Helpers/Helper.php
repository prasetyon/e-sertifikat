<?php

namespace App\Helper;

use App\Models\DokumenPengajuan;
use App\Models\Notification;
use App\Models\User;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class Helper
{
    public static function formatDate($date)
    {
        if (!$date) return null;
        $date = date_create($date);
        return date_format($date, "d M Y");
    }

    public static function formatTimestamp($timestamp)
    {
        $date = substr($timestamp, 0, 10);
        $time = substr($timestamp, 12);
        $date = date_create($date);
        return date_format($date, "d M Y") . ' ' . $time;
    }

    public static function deleteFile($file)
    {
        unlink(storage_path('app/public/' . substr(str_replace(env('APP_URL') . '/storage', "", $file), 1)));
        Storage::delete('app/public/' . substr(str_replace(env('APP_URL') . '/storage', "", $file), 1));
    }

    public static function getRandomColor()
    {
        return '#' . str_pad(dechex(mt_rand(0, 255)), 2, '0', STR_PAD_LEFT) .
            str_pad(dechex(mt_rand(0, 255)), 2, '0', STR_PAD_LEFT) .
            str_pad(dechex(mt_rand(0, 255)), 2, '0', STR_PAD_LEFT);;
    }
}
