<?php

namespace App\Http\Livewire;

use App\Exports\DataPengajuanExport;
use App\Helper\Helper;
use App\Models\DataPengajuan;
use App\Models\ProsesPetaBidang;
use App\Models\ProsesSertifikat;
use App\Models\ProsesSKHak;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;
use Maatwebsite\Excel\Facades\Excel;

class PengajuanComponent extends Component
{
    use WithPagination, WithFileUploads;
    protected $paginationTheme = 'bootstrap';

    // Public variable
    public $isOpenPengajuan = false;
    public $isOpenSKHak = false;
    public $isOpenSertifikat = false;
    public $isOpenPetaBidang = false;
    public $isOpenDetail = false;
    public $paginatedPerPages = 10;
    public $search;
    public $selectedData;
    public $pengajuanId, $req;
    public $input_id, $input_nama, $input_luas, $input_nilai, $input_alamat, $input_tahun;
    public $input_kode_barang, $input_kode_registrasi, $input_nomor_berkas, $input_hak;
    public $input_nomor_sertifikat, $input_tanggal_sertifikat, $input_pj, $input_keterangan;
    public $input_pengajuan, $input_rekening, $input_penjadwalan, $input_pengukuran, $input_bukti_bayar, $input_dokumen;

    public function mount($req = null, $id = null)
    {
        $this->pengajuanId = $id;
        $this->req = $req;
    }

    public function render()
    {
        $searchData = $this->search;
        $reqId = $this->pengajuanId;

        return view('web.pengajuan.pengajuan-component', [
            'lists' => DataPengajuan::with(['petaBidang', 'skHak', 'sertifikat'])
                ->when($this->req == 'kode', function ($query) use ($reqId) {
                    return $query->where('kode_barang', $reqId);
                })->when($this->req == 'status', function ($query) use ($reqId) {
                    return $query->where('status', $reqId);
                })->when($searchData, function ($searchQuery) use ($searchData) {
                    $searchQuery->where('nama', 'like', '%' . $searchData . '%')
                        ->orWhere('tahun', $searchData)
                        ->orWhere('alamat', 'like', '%' . $searchData . '%')
                        ->orWhere('kode_barang', 'like', '%' . $searchData . '%')
                        ->orWhere('kode_registrasi', 'like', '%' . $searchData . '%')
                        ->orWhere('nomor_sertifikat', 'like', '%' . $searchData . '%')
                        ->orWhere('pj', 'like', '%' . $searchData . '%')
                        ->orWhere('nilai', 'like', '%' . $searchData . '%')
                        ->orWhere('nomor_berkas_peta_bidang', 'like', '%' . $searchData . '%')
                        ->orWhere('nomor_berkas_sk_hak', 'like', '%' . $searchData . '%')
                        ->orWhere('nomor_berkas_sertifikat', 'like', '%' . $searchData . '%');
                })->orderBy('status', 'asc')->orderBy('created_at', 'desc')->paginate($this->paginatedPerPages),
        ])->extends('admin')
            ->layoutData([
                'title' => 'Pengajuan',
                'menu' => 'pengajuan',
            ]);
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    private function resetInputFields()
    {
        $this->reset([
            'input_id', 'input_nama', 'input_luas', 'input_nilai', 'input_alamat', 'input_tahun',
            'input_kode_barang', 'input_kode_registrasi', 'input_nomor_berkas', 'input_hak',
            'input_nomor_sertifikat', 'input_tanggal_sertifikat', 'input_pj', 'input_keterangan',
            'selectedData'
        ]);
    }

    public function resetDetailFields()
    {
        $this->reset([
            'input_pengajuan', 'input_rekening', 'input_penjadwalan', 'input_pengukuran',
            'input_bukti_bayar', 'input_dokumen', 'input_keterangan'
        ]);

        $this->isOpenPengajuan = false;
        $this->isOpenSKHak = false;
        $this->isOpenSertifikat = false;
        $this->isOpenPetaBidang = false;
        $this->isOpenDetail = true;

        $this->selectedData = DataPengajuan::where('id', $this->input_id)->first();
    }

    public function closeModal()
    {
        $this->resetInputFields();
        $this->resetDetailFields();

        $this->isOpenPengajuan = false;
        $this->isOpenSKHak = false;
        $this->isOpenSertifikat = false;
        $this->isOpenPetaBidang = false;
        $this->isOpenDetail = false;
    }

    public function openDetail($id)
    {
        $this->isOpenDetail = true;
        $this->input_id = $id;

        $this->selectedData = DataPengajuan::where('id', $id)->first();
    }

    public function approve($id)
    {
        DataPengajuan::where('id', $id)->update(['status' => '2']);
        $this->closeModal();
    }

    public function openSertifikat($data = null)
    {
        // dd($data);
        $this->isOpenSertifikat = true;

        if ($data != null) {
            $this->input_rekening = $data['rekening'];
            $this->input_keterangan = $data['keterangan'];
            $this->input_nomor_berkas = $data['nomor_berkas'];
        }
    }

    public function submitSertifikat()
    {
        $messages = [
            '*.required' => 'This column is required',
            '*.numeric' => 'This column is required to be filled in with number',
            '*.string' => 'This column is required to be filled in with letters',
        ];

        // Insert or Update if Ok
        ProsesSertifikat::updateOrCreate(['pengajuan' => $this->input_id], [
            'rekening' => $this->input_rekening,
            'nomor_berkas' => $this->input_nomor_berkas,
            'keterangan' => $this->input_keterangan
        ]);

        $storage = $this->selectedData->id;

        if ($this->input_bukti_bayar) {
            $fileName = 'BUKTI_BAYAR_SERTIFIKAT.pdf';

            $this->input_bukti_bayar->storeAs('public/' . $storage, $fileName);
            ProsesSertifikat::where('pengajuan', $this->input_id)->update([
                'bukti_bayar' => env('APP_URL') . '/storage/' . $storage . '/' . $fileName
            ]);
        }

        if ($this->input_dokumen) {
            $fileName = 'SERTIFIKAT.pdf';

            $this->input_dokumen->storeAs('public/' . $storage, $fileName);
            ProsesSertifikat::where('pengajuan', $this->input_id)->update([
                'sertifikat' => env('APP_URL') . '/storage/' . $storage . '/' . $fileName
            ]);
        }

        DataPengajuan::where('id', $this->input_id)
            ->update([
                'status' => $this->input_dokumen ? '2' : '1',
                'nomor_berkas_sertifikat' => $this->input_nomor_berkas
            ]);
        $this->alert('success', 'Data berhasil direkam');
        $this->resetDetailFields();
    }

    public function openSKHak($data = null)
    {
        // dd($data);
        $this->isOpenSKHak = true;

        if ($data != null) {
            $this->input_rekening = $data['rekening'];
            $this->input_penjadwalan = $data['penjadwalan'];
            $this->input_pengukuran = $data['penelitian'];
            $this->input_keterangan = $data['keterangan'];
            $this->input_nomor_berkas = $data['nomor_berkas'];
        }
    }

    public function submitSKHak()
    {
        $messages = [
            '*.required' => 'This column is required',
            '*.numeric' => 'This column is required to be filled in with number',
            '*.string' => 'This column is required to be filled in with letters',
        ];

        // Insert or Update if Ok
        ProsesSKHak::updateOrCreate(['pengajuan' => $this->input_id], [
            'rekening' => $this->input_rekening,
            'penjadwalan' => $this->input_penjadwalan,
            'penelitian' => $this->input_pengukuran,
            'nomor_berkas' => $this->input_nomor_berkas,
            'keterangan' => $this->input_keterangan
        ]);

        $storage = $this->selectedData->id;

        if ($this->input_bukti_bayar) {
            $fileName = 'BUKTI_BAYAR_SK_HAK.pdf';

            $this->input_bukti_bayar->storeAs('public/' . $storage, $fileName);
            ProsesSKHak::where('pengajuan', $this->input_id)->update([
                'bukti_bayar' => env('APP_URL') . '/storage/' . $storage . '/' . $fileName
            ]);
        }

        if ($this->input_dokumen) {
            $fileName = 'SK_HAK.pdf';

            $this->input_dokumen->storeAs('public/' . $storage, $fileName);
            ProsesSKHak::where('pengajuan', $this->input_id)->update([
                'sk_hak' => env('APP_URL') . '/storage/' . $storage . '/' . $fileName
            ]);
        }

        DataPengajuan::where('id', $this->input_id)
            ->update([
                'status' => '1',
                'nomor_berkas_sk_hak' => $this->input_nomor_berkas
            ]);
        $this->alert('success', 'Data berhasil direkam');
        $this->resetDetailFields();
    }

    public function openPetaBidang($data = null)
    {
        // dd($data);
        $this->isOpenPetaBidang = true;

        if ($data != null) {
            $this->input_rekening = $data['rekening'];
            $this->input_penjadwalan = $data['penjadwalan'];
            $this->input_pengukuran = $data['pengukuran'];
            $this->input_nomor_berkas = $data['nomor_berkas'];
            $this->input_keterangan = $data['keterangan'];
        }
    }

    public function submitPetaBidang()
    {
        $messages = [
            '*.required' => 'This column is required',
            '*.numeric' => 'This column is required to be filled in with number',
            '*.string' => 'This column is required to be filled in with letters',
        ];

        // Insert or Update if Ok
        ProsesPetaBidang::updateOrCreate(['pengajuan' => $this->input_id], [
            'rekening' => $this->input_rekening,
            'penjadwalan' => $this->input_penjadwalan,
            'pengukuran' => $this->input_pengukuran,
            'nomor_berkas' => $this->input_nomor_berkas,
            'keterangan' => $this->input_keterangan
        ]);

        $storage = $this->selectedData->id;

        if ($this->input_bukti_bayar) {
            $fileName = 'BUKTI_BAYAR_PETA_BIDANG.pdf';

            $this->input_bukti_bayar->storeAs('public/' . $storage, $fileName);
            ProsesPetaBidang::where('pengajuan', $this->input_id)->update([
                'bukti_bayar' => env('APP_URL') . '/storage/' . $storage . '/' . $fileName
            ]);
        }

        if ($this->input_dokumen) {
            $fileName = 'PETA_BIDANG.pdf';

            $this->input_dokumen->storeAs('public/' . $storage, $fileName);
            ProsesPetaBidang::where('pengajuan', $this->input_id)->update([
                'peta_bidang' => env('APP_URL') . '/storage/' . $storage . '/' . $fileName
            ]);
        }

        DataPengajuan::where('id', $this->input_id)
            ->update([
                'status' => '1',
                'nomor_berkas_peta_bidang' => $this->input_nomor_berkas
            ]);
        $this->alert('success', 'Data berhasil direkam');
        $this->resetDetailFields();
    }

    public function openPengajuan($id = null)
    {
        $this->isOpenPengajuan = true;

        if ($id != null) {
            $data = DataPengajuan::findOrFail($id);

            // Parse data from the $data variable
            $this->input_id = $id;
            $this->input_nama = $data->nama;
            $this->input_luas = $data->luas;
            $this->input_nilai = $data->nilai;
            $this->input_alamat = $data->alamat;
            $this->input_tahun = $data->tahun;
            $this->input_kode_barang = $data->kode_barang;
            $this->input_kode_registrasi = $data->kode_registrasi;
            $this->input_hak = $data->hak;
            $this->input_nomor_sertifikat = $data->nomor_sertifikat;
            $this->input_tanggal_sertifikat = $data->tanggal_sertifikat;
            $this->input_pj = $data->pj;
            $this->input_keterangan = $data->keterangan;
        }
    }

    public function submitPengajuan()
    {
        $messages = [
            '*.required' => 'This column is required',
            '*.numeric' => 'This column is required to be filled in with number',
            '*.string' => 'This column is required to be filled in with letters',
        ];

        $this->validate([
            'input_nama' => 'required|string',
            'input_luas' => 'required|numeric',
            'input_nilai' => 'required|numeric',
        ], $messages);

        // Insert or Update if Ok
        DataPengajuan::updateOrCreate(['id' => $this->input_id], [
            'nama' => $this->input_nama,
            'luas' => $this->input_luas,
            'alamat' => $this->input_alamat,
            'nilai' => $this->input_nilai,
            'tahun' => $this->input_tahun,
            'kode_barang' => $this->input_kode_barang,
            'kode_registrasi' => $this->input_kode_registrasi,
            'hak' => $this->input_hak,
            'nomor_sertifikat' => $this->input_nomor_sertifikat,
            'tanggal_sertifikat' => $this->input_tanggal_sertifikat,
            'pj' => $this->input_pj,
            'keterangan' => $this->input_keterangan,
        ]);

        $this->alert('success', $this->input_id ? 'Data berhasil diperbarui' : 'Data berhasil disimpan');
        $this->closeModal();
    }

    public function deleteProses($id, $proses)
    {
        if ($proses == 'sertifikat') $del = ProsesSertifikat::where('pengajuan', $id)->delete();
        else if ($proses == 'skhak') $del = ProsesSKHak::where('pengajuan', $id)->delete();
        else if ($proses == 'petabidang') $del = ProsesPetaBidang::where('pengajuan', $id)->delete();

        $data = DataPengajuan::where('id', $id)->first();
        if ($data->sertifikat && $data->sertifikat->sertifikat) $status = 2;
        else if (($data->skHak && $data->skHak->sk_hak) || ($data->petaBidang && $data->petaBidang->peta_bidang)) $status = 1;
        else $status = 0;

        DataPengajuan::where('id', $id)->update(['status' => $status]);

        $this->alert('success', 'Data berhasil dihapus');
        $this->resetDetailFields();
    }

    public function delete($id)
    {
        $del1 = ProsesSertifikat::where('pengajuan', $id)->delete();
        $del2 = ProsesSKHak::where('pengajuan', $id)->delete();
        $del3 = ProsesPetaBidang::where('pengajuan', $id)->delete();
        $del = DataPengajuan::where('id', $id)->delete();
        $this->alert('success', 'Data berhasil dihapus');
    }
}
