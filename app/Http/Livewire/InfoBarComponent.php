<?php

namespace App\Http\Livewire;

use App\Models\DataPengajuan;
use Livewire\Component;

class InfoBarComponent extends Component
{
    public $showTotal = 0;
    public $showSertif = 0;
    public $showProses = 0;

    public function render()
    {
        $data = DataPengajuan::with(['petaBidang', 'skHak', 'sertifikat'])->get();
        $total = 0;
        $sudahSertifikat = 0;
        $belumSertifikat = 0;
        $belumProses = 0;
        $sedangProses = 0;
        $petaBidang = 0;
        $skHak = 0;
        $sertifikat = 0;

        foreach ($data as $d) {
            if ($d->status == 2) {
                $sudahSertifikat++;
            } else if ($d->status == 0) {
                $belumProses++;
                $belumSertifikat++;
            } else {
                if ($d->sertifikat && $d->sertifikat->nomor_berkas) {
                    $sertifikat++;
                } else if ($d->skHak && $d->skHak->nomor_berkas) {
                    $skHak++;
                } else {
                    $petaBidang++;
                }
                $sedangProses++;
                $belumSertifikat++;
            }

            $total++;
        }

        return view('livewire.info-bar-component', [
            'total' => $total,
            'sudahSertifikat' => $sudahSertifikat,
            'belumSertifikat' => $belumSertifikat,
            'belumProses' => $belumProses,
            'sedangProses' => $sedangProses,
            'petaBidang' => $petaBidang,
            'skHak' => $skHak,
            'sertifikat' => $sertifikat,
        ]);
    }

    public function openTotal()
    {
        $this->showTotal = !$this->showTotal;
        $this->showSertif = 0;
        $this->showProses = 0;
    }

    public function openSertif()
    {
        $this->showTotal = 1;
        $this->showSertif = !$this->showSertif;
        $this->showProses = 0;
    }

    public function openProses()
    {
        $this->showTotal = 1;
        $this->showSertif = 1;
        $this->showProses = !$this->showProses;
    }
}
