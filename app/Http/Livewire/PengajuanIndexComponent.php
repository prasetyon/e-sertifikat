<?php

namespace App\Http\Livewire;

use App\Models\DataPengajuan;
use Livewire\Component;
use Livewire\WithPagination;

class PengajuanIndexComponent extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';

    public $search;
    public $paginatedPerPages = 10;

    public $isOpenPengajuan = false;
    public $input_id, $input_nama, $input_luas, $input_nilai, $input_alamat, $input_tahun;
    public $input_kode_barang, $input_kode_registrasi, $input_nomor_berkas, $input_hak;
    public $input_nomor_sertifikat, $input_tanggal_sertifikat, $input_pj, $input_keterangan;

    public function render()
    {
        $searchData = $this->search;
        return view('livewire.pengajuan-index-component', [
            'lists' => DataPengajuan::selectRaw('kode_barang, COUNT(kode_registrasi) as num')
                ->when($searchData, function ($searchQuery) use ($searchData) {
                    $searchQuery->where('kode_barang', 'like', '%' . $searchData . '%');
                })->groupBy('kode_barang')
                ->orderBy('kode_barang')
                ->paginate($this->paginatedPerPages)
        ])
            ->extends('admin')
            ->layoutData([
                'title' => 'Pengajuan',
                'menu' => 'pengajuan',
            ]);
    }

    private function resetInputFields()
    {
        $this->reset([
            'input_id', 'input_nama', 'input_luas', 'input_nilai', 'input_alamat', 'input_tahun',
            'input_kode_barang', 'input_kode_registrasi', 'input_hak',
            'input_nomor_sertifikat', 'input_tanggal_sertifikat', 'input_pj', 'input_keterangan'
        ]);
    }

    public function openPengajuan()
    {
        $this->isOpenPengajuan = true;
    }

    public function closeModal()
    {
        $this->resetInputFields();

        $this->isOpenPengajuan = false;
    }


    public function submitPengajuan()
    {
        $messages = [
            '*.required' => 'This column is required',
            '*.numeric' => 'This column is required to be filled in with number',
            '*.string' => 'This column is required to be filled in with letters',
        ];

        $this->validate([
            'input_nama' => 'required|string',
            'input_luas' => 'required|numeric',
            'input_nilai' => 'required|numeric',
        ], $messages);

        // Insert or Update if Ok
        DataPengajuan::updateOrCreate(['id' => $this->input_id], [
            'nama' => $this->input_nama,
            'luas' => $this->input_luas,
            'alamat' => $this->input_alamat,
            'nilai' => $this->input_nilai,
            'tahun' => $this->input_tahun,
            'kode_barang' => $this->input_kode_barang,
            'kode_registrasi' => $this->input_kode_registrasi,
            'hak' => $this->input_hak,
            'nomor_sertifikat' => $this->input_nomor_sertifikat,
            'tanggal_sertifikat' => $this->input_tanggal_sertifikat,
            'pj' => $this->input_pj,
            'keterangan' => $this->input_keterangan,
        ]);

        $this->alert('success', $this->input_id ? 'Data berhasil diperbarui' : 'Data berhasil disimpan');
        $this->closeModal();
    }
}
