<?php

namespace App\Http\Livewire;

use App\Exports\DataPengajuanExport;
use App\Imports\DataPengajuanImport;
use Livewire\Component;
use Livewire\WithFileUploads;
use Maatwebsite\Excel\Facades\Excel;

class ImportPengajuan extends Component
{
    use WithFileUploads;

    public $file;
    public $showModal = 0;

    public function render()
    {
        return view('livewire.import-pengajuan');
    }

    public function openModalImport()
    {
        $this->showModal = true;
    }

    public function closeModalImport()
    {
        $this->showModal = false;
    }

    public function exportPengajuan()
    {
        return Excel::download(new DataPengajuanExport, 'datapengajuan.xlsx');
    }

    public function importPengajuan()
    {
        $this->validate([
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);

        Excel::import(new DataPengajuanImport(), $this->file);

        $this->alert('success', 'Data Pengajuan berhasil diimport');
        $this->closeModalImport();
    }
}
