<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function index()
    {
        if (!Auth::check()) return view('login')->with('title', 'Login');
        else {
            $user = Auth::user();
            if ($user->role == 'superuser') return redirect()->route('dashboard');
            else return redirect()->route('data');
        }
    }

    public function login(Request $request)
    {
        $username = $request->username;
        $pwd = $request->password;

        if (Auth::attempt(['username' => $username, 'password' => $pwd])) {
            $user = Auth::user();
            if ($user->role == 'superuser') return redirect()->route('dashboard');
            else return redirect()->route('data');
        } else {
            return redirect()->back()->with('alert', 'Terjadi kesalahan dalam login. Cek kembali Username dan Password Anda');
        }
    }

    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect()->route('home');
    }
}
