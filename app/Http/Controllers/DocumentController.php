<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DocumentController extends Controller
{
    public function index(Request $request)
    {

        return view('web.document')
            ->with('title', 'Document')
            ->with('menu', 'document');
    }
    public function form(Request $request)
    {

        return view('web.form')
            ->with('title', 'Form')
            ->with('menu', 'document');
    }
}
