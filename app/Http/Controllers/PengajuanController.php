<?php

namespace App\Http\Controllers;

use App\Models\DataPengajuan;
use Illuminate\Http\Request;

class PengajuanController extends Controller
{
    public function index()
    {
        $data['data'] = DataPengajuan::orderBy('created_at', 'desc')->get();

        return view('web.pengajuan.index')
            ->with('data', $data)
            ->with('title', 'Pengajuan')
            ->with('menu', 'pengajuan');
    }

    public function create()
    {
        return view('web.pengajuan.create')
            ->with('title', 'Buat Pengajuan')
            ->with('menu', 'pengajuan');
    }

    public function store(Request $request)
    {
        $data = new DataPengajuan;
        $data->nama = $request->nama;
        $data->luas = $request->luas;
        $data->nilai = $request->nilai;
        $data->alamat = $request->alamat;
        $data->tahun = $request->tahun;
        $data->kode_barang = $request->kode_barang;
        $data->kode_registrasi = $request->kode_registrasi;
        $data->rencana = $request->rencana;
        $data->hak = $request->hak;
        $data->nomor_sertifikat = $request->nomor_sertifikat;
        $data->tanggal_sertifikat = $request->tanggal_sertifikat;
        $data->pj = $request->pj;
        $data->keterangan = $request->keterangan;
        $data->save();

        return redirect()->route('pengajuan.index')->with('alert-success', 'Pengajuan berhasil ditambahkan');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        $update = DataPengajuan::where('id', $id)
            ->update([
                'nama' => $request->nama,
                'luas' => $request->luas,
                'nilai' => $request->nilai,
                'alamat' => $request->alamat,
                'tahun' => $request->tahun,
                'kode_barang' => $request->kode_barang,
                'kode_registrasi' => $request->kode_registrasi,
                'rencana' => $request->rencana,
                'hak' => $request->hak,
                'nomor_sertifikat' => $request->nomor_sertifikat,
                'tanggal_sertifikat' => $request->tanggal_sertifikat,
                'pj' => $request->pj,
                'keterangan' => $request->keterangan
            ]);

        return redirect()->back()->with('alert-success', 'Pengajuan berhasil diperbarui');
    }

    public function destroy($id)
    {
        $delete = DataPengajuan::where('id', $id)->delete();

        return redirect()->route('pengajuan.index')->with('alert-success', 'Pengajuan berhasil dihapus');
    }
}
