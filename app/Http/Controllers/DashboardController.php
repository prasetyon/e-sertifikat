<?php

namespace App\Http\Controllers;

use App\Exports\DataPengajuanExport;
use App\Helper\Helper;
use App\Http\Controllers\Controller;
use App\Imports\DataPengajuanImport;
use App\Models\DataPengajuan;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        $data['pengajuan'] = DataPengajuan::orderBy('created_at', 'desc')->limit(10)->get();
        $rekapStatus = DataPengajuan::selectRaw('status, COUNT(id) as count')->groupBy('status')->get();
        $rekapJenis = DataPengajuan::selectRaw('nama, COUNT(id) as count')->groupBy('nama')->orderBy('count', 'desc')->get();
        $rekapPj = DataPengajuan::selectRaw('pj, COUNT(id) as count')->groupBy('pj')->orderBy('count', 'desc')->get();

        $data['jenisdata'] = [];
        $data['jenis'] = [];
        $data['jenislabel'] = [];
        $data['jeniscolor'] = [];
        $data['bardata'] = [];
        $data['bar'] = [];
        $data['barcolor'] = [];
        $data['barlabel'] = [];

        $data['rekap'] = [0 => 0, 1 => 0, 2 => 0];

        $totalcount = 0;
        foreach ($rekapStatus as $r) {
            $totalcount += $r->count;
            $data['rekap'][intval($r->status)] = $r->count;
        }

        foreach ($rekapPj as $r) {
            if ($r->count > 10) {
                $data['bardata'][] = $r->count;
                $data['barlabel'][] = $r->pj;
                $data['barcolor'][] = Helper::getRandomColor();
            }
        }

        $tempcount = 0;
        foreach ($rekapJenis as $r) {
            if (!$data['jenisdata'] || count($data['jenisdata']) < 3) {
                $tempcount += $r->count;
                $data['jenisdata'][] = $r->count;
                $data['jenislabel'][] = $r->nama;
                $data['jeniscolor'][] = Helper::getRandomColor();
            } else {
                $data['jenisdata'][] = $totalcount - $tempcount;
                $data['jenislabel'][] = 'Lainnya';
                $data['jeniscolor'][] = Helper::getRandomColor();
                break;
            }
        }

        $data['total'] = $totalcount;

        return view('web.index')
            ->with('data', $data)
            ->with('title', 'Dashboard')
            ->with('menu', 'dashboard');
    }

    public function exportPengajuan()
    {
        return Excel::download(new DataPengajuanExport, 'datapengajuan.xlsx');
    }

    public function importPengajuan(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);

        $state = $request->state === 'true' ? true : false;
        $file = $request->file('file');
        $destinationPath = public_path() . '/upload/file_pengajuan/';
        $uploadedname = rand() . $file->getClientOriginalName();

        $file->move($destinationPath, $uploadedname);

        Excel::import(new DataPengajuanImport($state), $destinationPath . $uploadedname);

        return redirect()->route('dashboard')->with('alert-success', 'Data Pengajuan berhasil diimport');
    }
}
