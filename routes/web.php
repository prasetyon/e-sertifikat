<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\DocumentController;
use App\Http\Livewire\PengajuanComponent;
use App\Http\Livewire\PengajuanCreateComponent;
use App\Http\Livewire\PengajuanIndexComponent;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login', [AuthController::class, 'index'])->name('login');
Route::post('login', [AuthController::class, 'login'])->name('login');
Route::get('logout', [AuthController::class, 'logout'])->name('logout');

Route::middleware('loggedin:admin')->group(function () {
    Route::any('/', [DashboardController::class, 'index'])->name('home');
    Route::any('dashboard', [DashboardController::class, 'index'])->name('dashboard');
    Route::any('document', [DocumentController::class, 'index'])->name('document');
    Route::any('form', [DocumentController::class, 'form'])->name('form');

    Route::get('pengajuan', PengajuanComponent::class)->name('pengajuan');
    Route::get('pengajuan/{req}/{id?}', PengajuanComponent::class)->name('detail');
});
